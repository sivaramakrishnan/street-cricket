import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MenuComponent} from './menu/menu.component';
import {NewMatchComponent} from './new-match/new-match.component';
import {ScorerComponent} from './scorer/scorer.component';
import {StatsComponent} from './stats/stats.component';
import {NoMatchDataComponent} from './no-match-data/no-match-data.component';
import {PgNotFoundComponent} from './pg-not-found/pg-not-found.component';
import {PageGuardGuard} from './page-guard.guard';

const routes: Routes = [
  {path: '', component: MenuComponent},
  {path: 'new', component: NewMatchComponent},
  {path: 'scorer', component: ScorerComponent, canActivate: [PageGuardGuard]},
  {path: 'stats', component: StatsComponent, canActivate: [PageGuardGuard]},
  {path: 'no-match-data', component: NoMatchDataComponent},
  {path: '**', component: PgNotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
