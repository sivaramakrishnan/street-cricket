import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MenuComponent } from './menu/menu.component';
import { NewMatchComponent } from './new-match/new-match.component';
import { ScorerComponent } from './scorer/scorer.component';
import { StatsComponent } from './stats/stats.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { PgNotFoundComponent } from './pg-not-found/pg-not-found.component';
import { NoMatchDataComponent } from './no-match-data/no-match-data.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    NewMatchComponent,
    ScorerComponent,
    StatsComponent,
    PgNotFoundComponent,
    NoMatchDataComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
