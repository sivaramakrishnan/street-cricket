import {Component, OnInit} from '@angular/core';
import {Match} from '../match';
import {AppService} from '../app.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-stats',
  template: `
    <div class="container mt-2 mb-3">
      <div class="row justify-content-center align-items-center p-2">
        <div class="m-1 col-sm-8 p-3 bg-white">
          <div class="p-3 pt-5 pb-5 shadow-sm border rounded">
            <h2>Stats</h2>
            <hr>
            <div class="row">
              <div class="col">
                <button type="button" class="btn btn-light m-2" [routerLink]="['/scorer']">Back to Scorer</button>
                <h4 class="mt-2 mb-3 font-weight-normal">1st Innings</h4>
                <ul class="list-unstyled">
                  <li *ngFor="let stats of match.innings1.overStats; index as i;">
                    Over {{i + 1}}:<br>
                    <pre><span class="mx-3">{{stats}}</span></pre>
                  </li>
                </ul>
                <div class="px-3" *ngIf="match.innings1.overStats.length === 0">
                  <p>No Stats Found</p>
                </div>
                <h4 class="mt-2 mb-3 font-weight-normal">2nd Innings</h4>
                <ul class="list-unstyled">
                  <li *ngFor="let stats of match.innings2.overStats; index as i;">
                    Over {{i + 1}}:<br>
                    <pre><span class="mx-3">{{stats}}</span></pre>
                  </li>
                </ul>
                <div class="px-3" *ngIf="match.innings2.overStats.length === 0">
                  <p>No Stats Found</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  `,
  styles: []
})
export class StatsComponent implements OnInit {
  public match: Match;

  constructor(private appService: AppService, private router: Router) {
  }

  ngOnInit(): void {
    const matchData = this.appService.getExistingMatch();
    if (!matchData) {
      this.router.navigate(['no-match-data']).then();
    }
    this.match = matchData;
  }
}
