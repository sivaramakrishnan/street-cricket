import {Innings} from './innings';

export class Match {
  public overs: number;
  public playersPerTeam: number;
  public innings1: Innings;
  public innings2: Innings;
  public isInningsOneEnded: boolean;
  public isInnings2Started: boolean;
  public isMatchEnded: boolean;
  public isLastManStanding: boolean;
  public isWideCount: boolean;
  public isNoBallCount: boolean;

  constructor(
    overs: number,
    playersPerTeam: number,
    innings1: Innings,
    innings2: Innings,
    isInningsOneEnded,
    isInnings2Started,
    isMatchEnded,
    isLastManStanding,
    isWideCount, isNoBallCount,
  ) {
    this.overs = overs;
    this.playersPerTeam = playersPerTeam;
    this.innings1 = innings1;
    this.innings2 = innings2;
    this.isInningsOneEnded = isInningsOneEnded;
    this.isInnings2Started = isInnings2Started;
    this.isMatchEnded = isMatchEnded;
    this.isLastManStanding = isLastManStanding;
    this.isWideCount = isWideCount;
    this.isNoBallCount = isNoBallCount;
  }
}
