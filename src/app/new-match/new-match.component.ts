import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {AppService} from '../app.service';

@Component({
  selector: 'app-new-match',
  template: `
    <div class="container mt-2 mb-3">
      <div class="row justify-content-center align-items-center p-2">
        <div class="m-1 col-sm-8 p-3 bg-white">
          <div class="p-3 pt-5 pb-5 shadow-sm border rounded">
            <h2>Preferences</h2>
            <hr>
            <form [formGroup]="preferenceForm" (ngSubmit)="onSubmit()">
              <div class="mb-3">
                <label for="overs" class="form-label">Number of overs</label>
                <input type="number" formControlName="overs" class="form-control" [ngClass]="{ 'is-invalid': submitted && f.overs.errors }"
                       id="overs" placeholder="Overs..">
                <div *ngIf="submitted && f.overs.errors" class="invalid-feedback">
                  <div *ngIf="f.overs.errors.required">First Name is required</div>
                  <div *ngIf="f.overs.errors.min">Choose at least 1 over</div>
                </div>
              </div>
              <div class="mb-3 mt-4">
                <label for="players" class="form-label">Number of players per team</label>
                <input type="number" formControlName="players" class="form-control"
                       [ngClass]="{ 'is-invalid': submitted && f.players.errors }" id="overs" placeholder="Players..">
                <div *ngIf="submitted && f.players.errors" class="invalid-feedback">
                  <div *ngIf="f.players.errors.required">First Name is required</div>
                  <div *ngIf="f.players.errors.min">Choose at least 2 players</div>
                </div>
              </div>
              <div class="mb-3 mt-4">
                <div class="form-check">
                  <input class="form-check-input" formControlName="lastMan" type="checkbox" value="" id="last-man"/>
                  <label class="form-check-label" for="last-man"> Last Man Standing</label>
                </div>
              </div>
              <div class="mb-3">
                <div class="form-check">
                  <input class="form-check-input" formControlName="countWide" type="checkbox" value="" id="count-wide"/>
                  <label class="form-check-label" for="count-wide"> Count Wide as Run</label>
                </div>
              </div>
              <div class="mb-3">
                <div class="form-check">
                  <input class="form-check-input" formControlName="countNoBall" type="checkbox" value="" id="count-no-ball"/>
                  <label class="form-check-label" for="count-no-ball"> Count No Ball as Run</label>
                </div>
              </div>
              <div class="text-center b-3">
                <button type="submit" class="btn btn-light">Let's Start</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  `,
  styles: []
})
export class NewMatchComponent implements OnInit {
  public preferenceForm: FormGroup;
  public submitted = false;

  constructor(private formBuilder: FormBuilder, private appService: AppService) {
  }

  ngOnInit(): void {
    this.preferenceForm = this.formBuilder.group({
      overs: [8, [Validators.required, Validators.min(1)]],
      players: [8, [Validators.required, Validators.min(2)]],
      lastMan: [true],
      countWide: [true],
      countNoBall: [true],
    });
  }

  // tslint:disable-next-line:typedef
  get f() {
    return this.preferenceForm.controls;
  }

  onSubmit(): void {
    this.submitted = true;
    if (this.preferenceForm.invalid) {
      return;
    }
    const formData = this.preferenceForm.value;
    this.appService.createNewMatch(
      formData.overs,
      formData.players,
      formData.lastMan,
      formData.countWide,
      formData.countNoBall
    );
  }

}
