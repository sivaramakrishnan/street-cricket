import {Component, OnInit} from '@angular/core';
import {AppService} from '../app.service';

@Component({
  selector: 'app-menu',
  template: `
    <div class="container mt-5 mb-3 text-center">
      <div class="row justify-content-center align-items-center text-center p-2">
        <div class="m-1 col-sm-8 p-3 bg-white">
          <div class="pt-5 pb-2 d-grid gap-2 col-6 mx-auto">
            <img class="rounded mx-auto d-block"
                 src="assets/cricket.png" alt=""
                 width=130px height=105px>
            <button class="btn btn-light mt-4" [routerLink]="['/new']">New Game</button>
            <button *ngIf="isMatchExists" class="btn btn-light mt-2" [routerLink]="['/scorer']">Continue</button>
            <button class="btn btn-light mt-2" (click)="goToAbout()">About</button>
            <button class="btn btn-light mt-2" (click)="clearAllData()">Clear Data</button>
          </div>
        </div>
      </div>
      <p [ngClass]="toastText"
         (animationend)="toastText.toast=false">cleared data.</p>
    </div>
  `,
  styles: [`
    .ts{
      color: green;
      opacity: 0;
    }
    .toast {
      box-shadow: none;
      outline: none;
      animation-iteration-count: 1;
      animation: pulse1 1s;
      animation-direction: alternate;
    }

    @keyframes pulse1 {
      0% {
        opacity: 0;
      }
      50% {
        opacity: 1;
      }
      100% {
        opacity: 0;
      }
    }
  `
  ]
})
export class MenuComponent implements OnInit {
  public isMatchExists: boolean;
  toastText = {
    ts: true,
    toast: false
  };

  constructor(private appService: AppService) {
    this.isMatchExists = this.isMatchAlreadyExists();
  }

  ngOnInit(): void {
  }

  goToAbout(): void {
    window.location.href = 'https://sivaramakrishnan.netlify.app';
  }

  isMatchAlreadyExists(): boolean {
    return this.appService.checkIfMatchExists();
  }

  clearAllData(): void {
    this.appService.eraseAllData();
    this.toastText.toast = true;
    this.isMatchExists = false;
  }
}
