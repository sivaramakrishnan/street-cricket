export class Innings {
  constructor(
    public isFirst: boolean,
    public oversUp: number,
    public ballsUp: number,
    public runs: number,
    public extras: number,
    public score: number,
    public wickets: number,
    public crr: number,
    public overStats: string[],
  ) {}
}
