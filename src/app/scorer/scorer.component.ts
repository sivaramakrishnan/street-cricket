import {Component, OnInit} from '@angular/core';
import {Match} from '../match';
import {AppService} from '../app.service';
import {Router} from '@angular/router';
import {Innings} from '../innings';
import {PlatformLocation} from '@angular/common';

@Component({
  selector: 'app-scorer',
  template: `
    <div class="container mt-2 mb-3">
      <div class="row justify-content-center align-items-center p-2">
        <div class="m-1 col-sm-8 p-3 bg-white">
          <div class="p-3 pt-5 pb-5 shadow-sm border rounded">
            <h2>Score</h2>
            <hr>
            <button type="button" class="btn btn-light m-2" [routerLink]="['/']">Main Menu</button>
            <div *ngIf="!match.isMatchEnded">
              <h4 class="mt-2 mb-3 font-weight-normal" *ngIf="!match.isInnings2Started">1st Innings</h4>
              <h4 class="mt-2 mb-3 font-weight-normal" *ngIf="match.isInnings2Started">2nd Innings</h4>
              <div class="row mb-4 border-bottom">
                <div class="offset-1 col">
                  <p class="display-6"><b><span [ngClass]="pulsingText1"
                                                (animationend)="pulsingText1.success=false">{{currentInnings['score']}}</span>/<span
                    [ngClass]="pulsingText2"
                    (animationend)="pulsingText2.failure=false">{{currentInnings['wickets']}}</span>
                    (<span>{{currentInnings['oversUp']}}</span>.<span>{{currentInnings['ballsUp']}}</span>)</b></p>
                  <p class="lead font-weight-normal d-inline" *ngIf="currentInnings.oversUp >= 1">CRR: <span
                    *ngIf="currentInnings['crr'] < 10">{{currentInnings['crr'] | number:'1.1-3'}}</span><span
                    *ngIf="currentInnings['crr'] >= 10">{{currentInnings['crr'] | number:'2.1-3'}}</span></p>
                  <p class="lead font-weight-normal d-inline" *ngIf="this.currentInnings['extras'] > 0"> Extras:
                    <span>{{currentInnings['extras']}}</span>
                  <p *ngIf="this.match.isInnings2Started && !this.match.isMatchEnded">Target: <span>{{match.innings1['score'] + 1}}</span>
                  </p>
                </div>
              </div>
              <div *ngIf="!(match.isInningsOneEnded && !match.isInnings2Started)" class="row mt-3">
                <div class="offset-1 col">
                  <div class="m-2 mb-3">
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" [(ngModel)]="noBall" id="no-ball"/>
                      <label class="form-check-label" for="no-ball"> No Ball</label>
                    </div>
                  </div>
                  <div class="m-2 mb-3">
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" [(ngModel)]="wicket" id="wicket"/>
                      <label class="form-check-label" for="wicket"> Wicket</label>
                    </div>
                  </div>
                  <button type="button" class="btn btn-light m-2" (click)="updateScore(1)">1</button>
                  <button type="button" class="btn btn-light m-2" (click)="updateScore(2)">2</button>
                  <button type="button" class="btn btn-light m-2" (click)="updateScore(3)">3</button>
                  <button type="button" class="btn btn-light m-2" (click)="updateScore(4)">4</button>
                  <button type="button" class="btn btn-light m-2" (click)="updateScore(6)">6</button>
                  <button type="button" class="btn btn-light m-2" (click)="updateScore(0)">0</button>
                  <button type="button" class="btn btn-light m-2" (click)="updateWide()">Wide</button>
                </div>
                <div class="col-1"></div>
              </div>
            </div>
            <div *ngIf="match.isMatchEnded" class="row">
              <div class="col">
                <h4 class="mt-2 mb-3 font-weight-normal">1st Innings</h4>
                <div class="offset-1 col">
                  <p class="display-6"><b><span>{{match.innings1['score']}}</span>/<span>{{match.innings1['wickets']}}</span>
                    (<span>{{match.innings1['oversUp']}}</span>.<span>{{match.innings1['ballsUp']}}</span>)</b></p>
                  <p class="lead font-weight-normal" *ngIf="match.innings1.oversUp >= 1">RR: <span
                    *ngIf="match.innings1['crr'] < 10">{{match.innings1['crr'] | number:'1.1-3'}}</span><span
                    *ngIf="match.innings1['crr'] >= 10">{{match.innings1['crr'] | number:'2.1-3'}}</span></p>
                </div>
                <h4 class="mt-2 mb-3 font-weight-normal">2nd Innings</h4>
                <div class="offset-1 col">
                  <p class="display-6"><b><span>{{match.innings2['score']}}</span>/<span>{{match.innings2['wickets']}}</span>
                    (<span>{{match.innings2['oversUp']}}</span>.<span>{{match.innings2['ballsUp']}}</span>)</b></p>
                  <p class="lead font-weight-normal" *ngIf="match.innings2.oversUp >= 1">RR: <span
                    *ngIf="match.innings2['crr'] < 10">{{match.innings2['crr'] | number:'1.1-3'}}</span><span
                    *ngIf="match.innings2['crr'] >= 10">{{match.innings2['crr'] | number:'2.1-3'}}</span></p>
                </div>
              </div>
            </div>
            <div class="row m-2">
              <div class="col">
                <button type="button" class="btn btn-light m-2" [routerLink]="['/stats']">Over Stats</button>
                <button type="button" class="btn btn-light m-2" *ngIf="isUndoAllowed" (click)="undoMatchData()">Undo</button>
                <button type="button" class="btn btn-light m-2" *ngIf="match.isInningsOneEnded && !match.isInnings2Started"
                        (click)="start2ndInnings()">Start 2nd
                  Innings
                </button>
                <button type="button" class="btn btn-light m-2" *ngIf="match.isMatchEnded" (click)="endTheMatch()">End Match</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  `,
  styles: [`
    .success {
      animation-iteration-count: 1;
      animation: pulse1 1s;
      animation-direction: alternate;
    }

    @keyframes pulse1 {
      0% {
        color: black
      }
      50% {
        font-size: 1.5em;
        color: green;
      }
      100% {
        color: black;
      }
    }

    .failure {
      animation-iteration-count: 1;
      animation: pulse2 1s;
      animation-direction: alternate;
    }

    @keyframes pulse2 {
      0% {
        color: black
      }
      50% {
        font-size: 1.5em;
        color: red;
      }
      100% {
        color: black;
      }
    }
  `]
})
export class ScorerComponent implements OnInit {
  public match: Match;
  public prevMatch: Match;
  public isPrevMatchExist: boolean;
  public currentInnings: Innings;
  public isUndoAllowed: boolean;
  noBall: boolean;
  wicket: boolean;
  ballStat: string;
  pulsingText1 = {
    success: false
  };
  pulsingText2 = {
    failure: false
  };

  constructor(private appService: AppService, private router: Router,
              // private location: PlatformLocation
  ) {
    this.noBall = false;
    this.wicket = false;
    this.ballStat = '';
    this.isPrevMatchExist = false;
    // this.location.onPopState(() => {
    //   history.pushState(null, null, window.location.href);
    // });
  }

  ngOnInit(): void {
    const matchData = this.appService.getExistingMatch();
    if (!matchData) {
      this.router.navigate(['no-match-data']).then();
    }
    this.match = matchData;
    this.currentInnings = this.getInnings();
    this.isUndoAllowed = this.isBackUpExists();
  }

  getInnings(): Innings {
    if (!(this.match.isInningsOneEnded === true && this.match.isInnings2Started === true)) {
      return this.match.innings1;
    } else {
      return this.match.innings2;
    }
  }

  updateBallStat(str: string): void {
    if (this.ballStat === '') {
      this.ballStat = str;
    } else {
      if (str === 'Wd') {
        if (this.ballStat === '0') {
          this.ballStat = 'Wd';
        } else {
          this.ballStat += '+Wd';
        }
      }
      if (str === 'Nb') {
        if (this.ballStat === '0') {
          this.ballStat = 'Nb';
        } else {
          this.ballStat += '+Nb';
        }
      }
      if (str === 'Ⓦ') {
        if (this.ballStat === '0') {
          this.ballStat = 'Ⓦ';
        } else {
          this.ballStat += '+Ⓦ';
        }
      }
    }
  }


  updateScore(runs: number): void {
    this.backupMatchData();
    let runsToAdd = runs;
    this.updateBallStat('' + runs);
    let wicketsToAdd = 0;
    let isBallCount = true;
    if (this.noBall) {
      this.updateBallStat('Nb');
      isBallCount = false;
      if (this.match.isNoBallCount === true) {
        this.currentInnings.extras++;
        runsToAdd++;
      }
    }
    if (this.wicket) {
      this.updateBallStat('Ⓦ');
      wicketsToAdd++;
    }
    if (wicketsToAdd > 0) {
    }
    this.updateScoreBoard(runsToAdd, wicketsToAdd, isBallCount);
  }

  updateWide(): void {
    this.backupMatchData();
    this.updateBallStat('Wd');
    let runsToAdd = 0;
    let wicketsToAdd = 0;
    if (this.match.isWideCount === true) {
      this.currentInnings.extras++;
      runsToAdd++;
    }

    if (this.wicket) {
      wicketsToAdd++;
    }
    this.updateScoreBoard(runsToAdd, wicketsToAdd, false);
  }

  updateScoreBoard(runs: number, wickets: number, isBallCount: boolean): void {
    if (typeof this.currentInnings.overStats[this.currentInnings.oversUp] === 'undefined') {
      this.currentInnings.overStats[this.currentInnings.oversUp] = this.ballStat + ' ';
    } else {
      this.currentInnings.overStats[this.currentInnings.oversUp] += this.ballStat + ' ';
    }
    this.ballStat = '';

    this.currentInnings.score += runs;
    if (runs > 0) {
      this.pulsingText1.success = true;
    }
    if (wickets > 0) {
      this.pulsingText2.failure = true;
    }
    this.currentInnings.wickets += wickets;
    if (isBallCount) {
      this.currentInnings.ballsUp++;
    }
    this.checkOverUpAndUpdate();
    this.calculateAndUpdateCRR();
    this.updateInningsInMatch();
    this.checkInningsUpAndUpdate();
    this.isUndoAllowed = this.isBackUpExists();
    this.appService.updateMatchData(this.match);
    this.noBall = false;
    this.wicket = false;
  }

  calculateAndUpdateCRR(): void {
    this.currentInnings.crr = this.currentInnings.score / this.currentInnings.oversUp;
  }

  checkOverUpAndUpdate(): void {
    if (this.currentInnings.ballsUp > 5) {
      this.currentInnings.oversUp++;
      this.currentInnings.ballsUp = 0;
    }
  }

  checkInningsUpAndUpdate(): void {
    let wicketsLimit = this.match.playersPerTeam - 1;
    if (this.match.isLastManStanding === true) {
      wicketsLimit = wicketsLimit + 1;
    }
    if (this.match.isInningsOneEnded !== true) {
      if ((this.currentInnings.oversUp === this.match.overs) || (this.currentInnings.wickets === wicketsLimit)) {
        this.match.isInningsOneEnded = true;
      }
    } else {
      if ((this.currentInnings.oversUp === this.match.overs) || (this.currentInnings.wickets === wicketsLimit) ||
        (this.match.innings2.score > this.match.innings1.score)) {
        this.match.isMatchEnded = true;
      }
    }
  }

  updateInningsInMatch(): void {
    if (this.match.isInningsOneEnded !== true) {
      delete this.match.innings1;
      this.match.innings1 = this.currentInnings;
    } else {
      delete this.match.innings2;
      this.match.innings2 = this.currentInnings;
    }
  }

  start2ndInnings(): void {
    this.currentInnings = this.match.innings2;
    this.match.isInnings2Started = true;
    localStorage.removeItem('prev');
    this.isUndoAllowed = this.isBackUpExists();
  }

  backupMatchData(): void {
    localStorage.setItem('prev', JSON.stringify(this.match));
  }

  undoMatchData(): void {
    const matchData = JSON.parse(localStorage.getItem('prev'));
    if (matchData !== null) {
      console.log(this.match);
      this.match = matchData;
      console.log(this.match);
      this.currentInnings = this.getInnings();
      this.appService.updateMatchData(this.match);
      localStorage.removeItem('prev');
      this.isUndoAllowed = this.isBackUpExists();
    } else {
    }
  }

  isBackUpExists(): boolean {
    if (localStorage.getItem('prev') !== null) {
      return true;
    } else {
      return false;
    }
  }

  endTheMatch(): void {
    this.appService.eraseMatchData();
    localStorage.removeItem('prev');
  }
}
