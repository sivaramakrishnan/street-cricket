import {Injectable} from '@angular/core';
import {Match} from './match';
import {Innings} from './innings';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  private readonly data;

  constructor(private router: Router) {
    this.data = this.fetchData();
  }

  // Used in AppService only
  fetchData(): any {
    // Use this line to reset data in localStorage
    // localStorage.setItem('data', JSON.stringify({isMatchDataExists: false}));
    if (localStorage.getItem('data') == null) {
      return {isMatchDataExists: false};
    } else {
      return JSON.parse(localStorage.getItem('data'));
    }
  }

  // Used in AppService only
  checkIfMatchExists(): boolean {
    if (this.data.isMatchDataExists === true) {
      return true;
    } else {
      return false;
    }
  }

  // Used in AppService only
  clearMatch(): void {
    if (this.data.match) {
      delete this.data.match;
    }
  }


  // Used in NewMatchComponent only
  createNewMatch(overs: number, playersPerTeam: number, lastManStanding: boolean, wideCount: boolean, noBallCount: boolean): void {
    const innings1 = new Innings(true, 0, 0, 0, 0, 0, 0, 0, []);
    const innings2 = new Innings(false, 0, 0, 0, 0, 0, 0, 0, []);
    const match: Match = new Match(overs, playersPerTeam, innings1, innings2, false, false, false, lastManStanding, wideCount, noBallCount);
    this.clearMatch();
    this.data.match = match;
    this.data.isMatchDataExists = true;
    localStorage.setItem('data', JSON.stringify(this.data));
    this.router.navigate(['scorer']).then();
  }

  //  Used in ScorerComponent and StatsComponent only
  getExistingMatch(): any {
    if (this.checkIfMatchExists()) {
      const match: Match = this.data.match;
      return match;
    } else {
      return {};
    }
  }

  // Used in ScorerComponent only
  updateMatchData(match: Match): void {
    this.clearMatch();
    this.data.match = match;
    localStorage.setItem('data', JSON.stringify(this.data));
  }

  eraseMatchData(): void {
    this.clearMatch();
    this.data.isMatchDataExists = false;
    localStorage.setItem('data', JSON.stringify(this.data));
    this.router.navigate(['']).then();
  }

  eraseAllData(): void {
    localStorage.removeItem('data');
    localStorage.removeItem('prev');
    this.router.navigate(['']).then();
  }
}
