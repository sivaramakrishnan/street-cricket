import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NoMatchDataComponent } from './no-match-data.component';

describe('NoMatchDataComponent', () => {
  let component: NoMatchDataComponent;
  let fixture: ComponentFixture<NoMatchDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NoMatchDataComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NoMatchDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
