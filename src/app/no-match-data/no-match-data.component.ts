import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-no-match-data',
  template: `
    <div class="container mt-5 mb-3">
      <div class="row justify-content-center align-items-center text-center p-2">
        <div class="m-1 col-sm-8 p-3 bg-white">
          <div class="pt-5 pb-5 d-grid gap-2 col-6 mx-auto">
            <img class="rounded mx-auto d-block mb-3"
                 src="assets/cricket.png" alt=""
                 width=130px height=105px>
            <p class="display-6">No Match Data</p>
            <button class="btn btn-light mt-1" [routerLink]="['/new']">Create New Match</button>
          </div>
        </div>
      </div>
    </div>
  `,
  styles: []
})
export class NoMatchDataComponent implements OnInit {

  constructor() {
  }

  ngOnInit(): void {
  }

}

